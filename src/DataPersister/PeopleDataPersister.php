<?php


namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\People;
use App\Manager\AbstractManager;
use App\Repository\AbastractServiceEntityRepository;

/**
 * Class PeopleDataPersister
 * @package App\DataPersister
 * @author sylvainjust
 */
class PeopleDataPersister implements DataPersisterInterface
{
    private $repository;
    private $manager;

    /**
     * PeopleDataPersister constructor.
     * @param AbastractServiceEntityRepository $repository
     */
    public function __construct(AbstractManager $manager, AbastractServiceEntityRepository $repository)
    {
        $this->manager = $manager;
        $this->repository = $repository;
    }

    /**
     * Is the data supported by the persister?
     */
    public function supports($data): bool
    {
        return $data instanceof People;
    }

    /**
     * Persists the data.
     *
     * @return object (Void will not be supported in API Platform 3, an object should always be returned)
     */
    public function persist($data)
    {
        dump($data);
        $entity = $data;
        if (is_null($data->getId())) {
            $entity = $this->manager->createEntityFromData($data);
        }
        $this->repository->save($entity);
        return $entity;
    }

    /**
     * Removes the data.
     */
    public function remove($data)
    {
        $this->repository->remove($data);
    }
}