<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190102230300 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-02 00:11:00', 1,1,'Si ça s’trouve ? Alors pour nous sortir de là il va nous falloir un peu plus solide que du si ça s’trouve !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-03 00:11:00', 1,1,'[Dame Séli : La pêche, les tartes, tout ça c est du patrimoine] (Arthur, montrant la tarte) C est du patrimoine ça ?')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-01 00:11:00', 1,1,'Cassez-vous ! Cassez-vous ! Cassez-vous ! Décarrez d’chez moi, bande de clampins ! Taillez-vous vite fait ! Et j’vous conseille de vous mettre au turbin, vous m’entendez ? Et le prochain qui se pointe avec un prototype, un vase à fleurs ou le pot de chambre de sa mamie, j’l’envoie garder les moutons dans les Highlands, pigé ?! Et tenez, reprenez vos merdes ! J suis pas vide-grenier !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-18 00:11:00', 1,2,'On se fait couper les deux bras, on revient vous voir et 5 minutes après on retourne se mettre sur la gueule ! Mettez-vous à la place des ennemis, c est désespérant.')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-19 00:11:00', 1,2,'Non, moi j crois qu il faut qu’vous arrêtiez d essayer d dire des trucs. Ça vous fatigue, déjà, et pour les autres, vous vous rendez pas compte de c que c est. Moi quand vous faites ça, ça me fout une angoisse... j pourrais vous tuer, j crois. De chagrin, hein ! J vous jure c est pas bien. Il faut plus que vous parliez avec des gens.')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-20 00:11:00', 1,2,'Quand je viens vous voir, pour vous demander des trucs ; c est l enchanteur que je viens voir ; pour qu il m donne des solutions d enchanteur : pas des combines à la noix ou des remèdes de bonne femme ! Vous êtes mon enchanteur, vous êtes pas ma grand-mère, OK ?')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-04 00:11:00', 1,3,'Y a pas à dire, dès qu il y a du dessert, le repas est tout de suite plus chaleureux !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-05 00:11:00', 1,3,'Qu est ce qu ils foutent ces cons de Saxons ?')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-06 00:11:00', 1,3,'Mon père, il n était pas ébouriffé, déjà, hein, il avait une coupe à la con mais c était plutôt aplati et puis il était pas vaporeux, voilà ! Allez, au lit !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-08 00:11:00', 1,3,'Mais Bohort, j vais vous faire mettre au cachot [...]. Non mais j vous écoute, j vous écoute seulement j vous préviens, j vous l dis, j vais vous faire descendre en cabane avec un pichet de flotte et un bout de pain sec. J suis désolé, j suis démuni, j vois pas d autre solution. Et puis j pense que ça vous donnera un peu l occasion de... de réfléchir un peu à tout ça à tête reposée, de prendre un peu d recul sur les choses parce que Bohort, on n réveille pas son roi en pleine nuit pour des conneries, encore moins deux fois d suite...')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-09 00:11:00', 1,3,'(À Merlin) C’est vrai ce qu’on dit, vous êtes le fils d’un démon et d’une pucelle ? […] Vous avez plus pris de la pucelle.')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-10 00:11:00', 1,1,'Mais ils s débrouillent ! Une relève toutes les deux s maines, j me fous pas d eux quand même ! C est ça monter la garde, hein, j suis désolé. On n a jamais dit qu c était une sinécure...')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-12 00:11:00', 1,1,'Il commence à doucement me faire chier celui là aussi!')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-11 00:11:00', 1,1,'Nouvelle technique : on passe pour des cons, les autres se marrent, et on frappe. C’est nouveau. […] Ah non, ça c’est que nous. Parce qu’il faut être capable de passer pour des cons en un temps record. Ah non, là-dessus on a une avance considérable.')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-16 00:11:00', 1,2,'(À Perceval, à propos de Merlin) d un moment, il est vraiment druide, c mec-là, ou ça fait quinze ans qu il me prend pour un con ?')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-13 00:11:00', 1,2,'Sortez-vous les doigts du cul !!!')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-14 00:11:00', 1,2,'n… nous, on mange quand on a faim.')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-15 00:11:00', 1,2,'Ah, mais des torches pareilles, on devrait les mettre sous verre, hein !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-17 00:11:00', 1,2,'(Plantant la seringue dans le bras de Merlin)Ah ! ben ça… i faut se méfier, avec les mecs à cran !…')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-21 00:11:00', 1,2,'(À Guenièvre)La prochaine fois que vous faites venir un barde, je lui ouvre le bide de là à là, j lui sors les boyaux et je file sa langue à bouffer aux chiens. C est clair, ça ?')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-22 00:11:00', 1,2,'Ça vous fait pas mal à la tête de glandouiller vingt-quatre heures sur vingt-quatre ?')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-23 00:11:00', 1,2,'Oh, mais c est pas vrai, mais vous allez m gonfler jusqu à quand ? Une heure que j crapahute dans tout le château avec ma bougie, ça va bien, maintenant ! ')");

        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-02 00:11:00', 2,2,'HAHA, Sire ! Je vous attends ! À moins que vous préfériez que l’on dise partout que le roi est une petite pédale qui pisse dans son froc à l’idée de se battre !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-03 00:11:00', 2,2,'Sire ! Mon père est peut-être unijambiste, mais moi, ma femme n a pas de moustache ! […] Alors ça vient? p tite bite !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-02 00:11:00', 2,2,'En garde, espèce de vieille pute dégarnie !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-04 00:11:00', 2,1,'JE NE MANGE PAS DE GRAINES !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-02 00:11:00', 2,1,'J estime que si on avale l équivalent de son poids en viande deux fois par jour, il ne faut pas s étonner de ne pas pouvoir mettre un pied devant l autre sur un champ de bataille.')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-07 00:11:00', 2,1,'Moi, une fois, j étais soûl comme cochon, je me suis fait tatouer J aime le raisin de table sur la miche droite, et ça y est toujours !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-02 00:11:00', 2,2,'Non, je veux dire « malade mental », c est votre maximum, comme insulte ? Non parce qu il va falloir passer le cran au-dessus, mon vieux, parce que sinon, on y est encore demain !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-09 00:11:00', 2,2,'En garde, ma biquette ! Je vais vous découper le gras du cul, ça vous fera ça de moins à trimbaler !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-02 00:11:00', 2,2,'(À Grüdü) Quand on est idiot, on plante des carottes on ne s occupe pas de sécurité !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-05 00:11:00', 2,3,'(Sur l air de À la Volette) Le bon roi Arthur est une p tite tapette. Le bon roi Arthur est une p tite tapette. Est une p tite à la volette, est une p tite à la volette, est une p tite tapette.')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-08 00:11:00', 2,3,'Du nerf, mon lapinou !… Vous allez vous faire tailler le zizi en pointe !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-02 00:11:00', 2,3,'ALLEZ, EN GARDE GROSSE CONNE ! Non, ça va pas, ça va pas !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-02 00:11:00', 2,3,'Non mais c est à se coincer les parties dans une porte !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-12 00:11:00', 2,3,'Vous savez quoi, Sire ? On va commencer par se faire une saucisse grillée de trois pieds de long, avec un tonnelet de pinard chacun, et derrière, peut être bien qu on se paiera des filles. Ah oui ! A un moment, vive la vie !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-02 00:11:00', 2,3,'(À Bohort) Mais allez-y bon sang, magnez-vous le fion, espèce de grosse dinde !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-02 00:11:00', 2,3,'Seigneur Bohort! Je commence à en avoir ras le bol de votre comportement de péteux alors vous allez me faire le plaisir de me faire une bonne insulte et de vous foutre en rogne une bonne fois pour toutes!')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-22 00:11:00', 2,1,'Vous êtes attendus aux archives. Vous, vous, vous je croyais que vous étiez mort, et vous ... me souviens plus ! Voilà ! Zut !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-02 00:11:00', 2,1,'Euh, juste une chose... Manquez encore une seule fois de respect au futur roi de Bretagne, et je vous coupe les boules ! Ca vous fera une jolie petite sacoche pour ranger vos dés à coudre.')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-22 00:11:00', 2,1,'(Manilius : Et ben je suis euh...) Je suis, je suis, je suis une petite tapette, qui parle à tort et à travers, sans que personne ne lui demande son avis, alors elle ferme son bec la poupoule... Et elle laisse parler les grands garçons.')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-02 00:11:00', 2,1,'Regardez moi la jolie petite paire de fillettes, si c est pas fragile!')");

        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-02 00:11:00', 3,3,'Si on faisait le coup du bouclier humain ?[...] Par exemple, Sire, Léodagan et moi on fait semblant de vous prendre en otage : on vous met une dague sous le cou et on traverse le camp adverse en gueulant :   Bougez pas, bougez pas ou on bute le roi! ...')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-01 00:11:00', 3,3,'Putain, en plein dans sa mouille !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-02-01 00:11:00', 3,3,'Faut faire comme avec les scorpions qui se suicident quand ils sont entourés par le feu, faut faire un feu en forme de cercle, autour d’eux, comme ça ils se suicident, pendant que nous on fait le tour et on lance de la caillasse de l’autre côté pour brouiller... Non ?')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-03 00:11:00', 3,3,'C’est pas faux.')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-01 00:11:00', 3,3,'Toi, un jour, je te crâme ta famille, toi.')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-03-01 00:11:00', 3,1,'Faut arrêter ces conneries de nord et de sud ! Une fois pour toutes, le nord, suivant comment on est tourné, ça change tout !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-02 00:11:00', 3,3,'Donc, pour résumer, je suis souvent victime des colibris, sous-entendu des types qu’oublient toujours tout. Euh, non… Bref, tout ça pour dire, que je voudrais bien qu’on me considère en tant que Tel.')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-01 00:11:00', 3,4,'(Entrant subitement dans la Salle de la Table Ronde) Excusez, c’est juste pour vous dire que je vais pas pouvoir rester aujourd’hui ! Faut que je retourne à la ferme de mes vieux ! Y a ma grand-mère qui a glissé sur une bouse ! C’est le vrai merdier !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-05-01 00:11:00', 3,4,'On a même un tabouret ! Quand on s’assoit d’ssus, on se retrouve sur un autre tabouret dans une taverne dans le Languedoc [...] Ouais, le siège de transport qu’ils appellent. En plus, comme par hasard c’est moi qui ai essayé le premier. Deux semaines et demi plus le bateau qu’ça m’a pris pour revenir. J’avais pas compris qu’en me rasseyant d’ssus, ça me ramenait de l’aut’côté. Et à l’arrivée j’me suis fait mettre une chasse, parce que j’avais ramené l’autre tabouret, et que soit-disant il aurait fallu qu’il reste là-bas. Pourtant ils marchent les deux tabourets ! Eh ben ils sont l’un à côté de l’autre. Alors ça fait pas pareil.')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-01 00:11:00', 3,1,'C’est pour ça : j’lis jamais rien. C’est un vrai piège à cons c’t’histoire-là. En plus j’sais pas lire.')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-01 00:11:00', 3,4,'Ben si, si c’est l’même volume sonore, on dit  équidistant  [...] S’ils sont équidistants en même temps que nous, on peut repérer le dragon par rapport à une certaine distance. Si le dragon s’éloigne, on s’ra équidistant, mais ça s’ra vachement moins précis et... et pas réciproque.')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-09 00:11:00', 3,5,'(À Arthur) Sire, avec tout le respect, est-ce que la reine a les fesses blanches ?')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-04-07 00:11:00', 3,5,'(À Arthur et Lancelot) Ça sert à rien, un siège, si elle est enceinte, il faut des linges blancs et une bassine d eau chaude.')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-09-01 00:11:00', 3,1,'(À Arthur et Lancelot) Une fois, à une exécution, je m approche d une fille. Pour rigoler, je lui fais : « Vous êtes de la famille du pendu ? »... C était sa sœur. Bonjour l approche !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-08 00:11:00', 3,5,'Si Joseph d Arimathie a pas été trop con, vous pouvez être sûr que le Graal, c est un bocal à anchois. ')");

        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-01 00:11:00', 4,4,'Je me sens la fureur de l’ours ! […] Je me sens le courage de l’oiseau ! […] Je suis sautillant comme le mulot ! […] Je frétille comme une p’tite truite ! […] J’ai la vigueur d’un insecte !')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-02-04 00:11:00', 4,1,'Et à un moment... Le sorcier s est mis à nous menacer, avec ses parties génitales.')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-03-01 00:11:00', 4,4,'Mon oncle, n est-ce pas vous même qui m avez dit que rien ne remplaçait une véritable amitié et qu en cas d urgence c était la seule corde à laquelle on pouvait vraiment se raccrocher ?.')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-03 00:11:00', 4,4,'Noblesse bien remisée ne trouve jamais l hiver à sa porte... Non, porte close...')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-04-01 00:11:00', 4,5,'Nous sommes jeunes, nous marchons à pieds… J’opterai donc pour un surnom en rapport :  les Petits Pédestres .')");
        $this->addSql("INSERT INTO message (datetime, people_id, wall_id, content) VALUES ('2018-01-06 00:11:00', 4,1,'Seigneur Bohort, pouvons-nous nous retirer afin d aller prendre notre goûter ?kkkkkkjjjk ')");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DELETE FROM message');
        $this->addSql("UPDATE sqlite_sequence SET seq = 1 WHERE name = 'message'");
    }
}
