<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181226141355 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql("CREATE TABLE wall (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, people_id INTEGER NOT NULL, CONSTRAINT FK_13F5EFF63147C936 FOREIGN KEY (people_id) REFERENCES people (id) NOT DEFERRABLE INITIALLY IMMEDIATE)");
        $this->addSql("INSERT INTO wall VALUES(1,1)");
        $this->addSql("INSERT INTO wall VALUES(2,2)");
        $this->addSql("INSERT INTO wall VALUES(3,3)");
        $this->addSql("INSERT INTO wall VALUES(4,4)");
        $this->addSql("INSERT INTO wall VALUES(5,5)");
        $this->addSql("INSERT INTO wall VALUES(6,6)");
        $this->addSql("CREATE TABLE message (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, people_id INTEGER NOT NULL, wall_id INTEGER NOT NULL, datetime DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, content CLOB DEFAULT NULL, CONSTRAINT FK_B6BD307F3147C936 FOREIGN KEY (people_id) REFERENCES people (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_B6BD307FC33923F1 FOREIGN KEY (wall_id) REFERENCES wall (id) NOT DEFERRABLE INITIALLY IMMEDIATE)");
        $this->addSql("INSERT INTO message VALUES(1,1,1,'2018-12-21 17:02:54','Si ca s trouve ? Alors pour nous sortir de la il va nous falloir un peu plus solide que du si ca s trouve')");
        $this->addSql("CREATE TABLE people (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username VARCHAR(255) NOT NULL COLLATE BINARY, password CLOB NOT NULL)");
        $this->addSql("INSERT INTO people VALUES(1,'sylvain','$2y$13$7mHpG5NReVkcyDjXAQKeted0hSKoqa5LD/q.Ces.S59YncvvKC3vi')");
        $this->addSql("INSERT INTO people VALUES(2,'renaud','$2y$13$7mHpG5NReVkcyDjXAQKeted0hSKoqa5LD/q.Ces.S59YncvvKC3vi')");
        $this->addSql("INSERT INTO people VALUES(3,'cedric','$2y$13$7mHpG5NReVkcyDjXAQKeted0hSKoqa5LD/q.Ces.S59YncvvKC3vi')");
        $this->addSql("INSERT INTO people VALUES(4,'foobar','$2y$13$7mHpG5NReVkcyDjXAQKeted0hSKoqa5LD/q.Ces.S59YncvvKC3vi')");
        $this->addSql("INSERT INTO people VALUES(5,'arthur','$2y$13$7mHpG5NReVkcyDjXAQKeted0hSKoqa5LD/q.Ces.S59YncvvKC3vi')");
        $this->addSql("INSERT INTO people VALUES(6,'sandrine','$2y$13$7mHpG5NReVkcyDjXAQKeted0hSKoqa5LD/q.Ces.S59YncvvKC3vi')");
        $this->addSql("CREATE UNIQUE INDEX UNIQ_28166A26F85E0677 ON people (username)");
        $this->addSql("CREATE TABLE people_people (people_source INTEGER NOT NULL, people_target INTEGER NOT NULL, PRIMARY KEY(people_source, people_target), CONSTRAINT FK_5E0148CC299D5D99 FOREIGN KEY (people_source) REFERENCES people (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_5E0148CC30780D16 FOREIGN KEY (people_target) REFERENCES people (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)");
        $this->addSql("INSERT INTO people_people VALUES(1,2)");
        $this->addSql("INSERT INTO people_people VALUES(2,1)");
        $this->addSql("DELETE FROM sqlite_sequence");
        $this->addSql("INSERT INTO sqlite_sequence VALUES('wall',6)");
        $this->addSql("INSERT INTO sqlite_sequence VALUES('message',1)");
        $this->addSql("INSERT INTO sqlite_sequence VALUES('people',6)");
        $this->addSql("CREATE UNIQUE INDEX UNIQ_13F5EFF63147C936 ON wall (people_id)");
        $this->addSql("CREATE INDEX IDX_B6BD307FC33923F1 ON message (wall_id)");
        $this->addSql("CREATE INDEX IDX_B6BD307F3147C936 ON message (people_id)");
        $this->addSql("CREATE INDEX IDX_5E0148CC30780D16 ON people_people (people_target)");
        $this->addSql("CREATE INDEX IDX_5E0148CC299D5D99 ON people_people (people_source)");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE wall');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE people');
        $this->addSql('DROP TABLE people_people');
    }
}
