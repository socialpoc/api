<?php


namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\Exception\ResourceClassNotSupportedException;
use App\Entity\People;
use App\Repository\AbastractServiceEntityRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class PeopleDataProvider
 * @package App\DataProvider
 * @author sylvainjust
 */
class PeopleDataProvider implements ItemDataProviderInterface
{

    private $tokenStorage;
    private $repository;

    public function __construct(AbastractServiceEntityRepository $repository, TokenStorageInterface $tokenStorage)
    {
        $this->repository = $repository;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Retrieves an item.
     *
     * @param array|int|string $id
     *
     * @throws ResourceClassNotSupportedException
     *
     * @return object|null
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        if (People::class !== $resourceClass) {
            throw new ResourceClassNotSupportedException();
        }
        dump($id);
        dump($operationName);
        if ('people_me' === $operationName) {
            $token = $this->tokenStorage->getToken();
            if (null === $token) {
                return null;
            }
            dump($token->getUser());
            return $token->getUser();
        }
        return $this->repository->find($id);
    }
}