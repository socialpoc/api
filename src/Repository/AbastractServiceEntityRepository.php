<?php


namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\ORMException;

/**
 * Class AbastractServiceEntityRepository
 * @package App\Repository
 * @author sylvainjust
 */
abstract class AbastractServiceEntityRepository extends ServiceEntityRepository
{

    /**
     * AbastractServiceEntityRepository constructor.
     * @param ManagerRegistry $registry
     * @param $entityClass
     */
    public function __construct(ManagerRegistry $registry, $entityClass)
    {
        parent::__construct($registry, $entityClass);
    }

    /**
     * @param Object $entity
     *
     * @return Object
     */
    public function save($entity)
    {
        try {
            $this->_em->persist($entity);
            $this->_em->flush();
        } catch (ORMException $e) {
            return null;
        }
        return $entity;
    }

    /**
     * @param $entity
     * @return bool
     */
    public function remove($entity)
    {
        try {
            $this->_em->remove($entity);
        $this->_em->flush();
        } catch (ORMException $e) {
            return false;
        }
        return true;
    }


}