<?php


namespace App\Manager;

use App\Entity\AbstractEntity;

/**
 * Class AbstractManager
 * @package App\Manager
 * @author sylvainjust
 */
abstract class AbstractManager
{

    /**
     * @param $data
     * @return AbstractEntity
     */
    abstract public function createEntityFromData($data);

}