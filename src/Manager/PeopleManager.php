<?php


namespace App\Manager;

use App\Entity\People;
use App\Entity\Wall;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class PeopleManager
 * @package App\People
 * @author sylvainjust
 */
class PeopleManager extends AbstractManager
{

    /**
     * @var \Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface
     */
    private $encoder;

    /**
     * PeopleManager constructor.
     * @param EncoderFactoryInterface $encoderFactory
     */
    public function __construct(EncoderFactoryInterface $encoderFactory)
    {
        $this->encoder = $encoderFactory->getEncoder(UserInterface::class);
    }

    /**
     * @param $data
     * @return \App\Entity\AbstractEntity
     */
    public function createEntityFromData($data)
    {
        $people = new People();
        $people->setPassword($this->encoder->encodePassword($data->getPassword(), $people->getSalt()));
        $people->setUsername($data->getUsername());
        $people->setWall(new Wall());
        return $people;
    }

}