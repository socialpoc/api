<?php


namespace App\Controller;

use App\Entity\People;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PeopleApiController
 * @package App\Controller
 * @author sylvainjust
 */
class PeopleApiController
{
    /**
     * @Route(
     *     name="app_current_people",
     *     path="/api/me",
     *     defaults={
     *         "_api_resource_class"=People::class,
     *         "_api_item_operation_name"="people_me",
     *         "id"="null"
     *     },
     *     methods={"GET"}
     * )
     * @param People $data
     * @return People
     */
    public function __invoke(People $data): People
    {
        return $data;
    }


}