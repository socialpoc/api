<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SecurityController
 * @package App\Controller
 * @author sylvainjust
 */
class SecurityController extends AbstractController
{

    /**
     * @Route(name="app_security_login_check", path="/api/login_check")
     */
    public function login_check()
    {
        throw new \LogicException('Please check your security.yaml config file.');
    }

    /**
     * @Route(name="app_security_login", path="/api/login")
     */
    public function login()
    {
        throw new \LogicException('Please check your security.yaml config file.');
    }

}