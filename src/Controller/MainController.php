<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MainController
 * @package App\Controller
 * @author sylvainjust
 */
class MainController extends AbstractController
{

    /**
     * @Route(name="app_main_index", path="/")
     */
    public function index()
    {
        return $this->redirectToRoute('api_entrypoint');
    }

}