<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class AbstractEntity
 * @package App\Entity
 * @author sylvainjust
 */
abstract class AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"all"})
     */
    protected $id;

    public function getId(): ?int
    {
        return $this->id;
    }


}