<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(normalizationContext={"groups"={"all", "wall"}})
 * @ORM\Entity(repositoryClass="App\Repository\WallRepository")
 */
class Wall extends AbstractEntity
{
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Message", mappedBy="wall", orphanRemoval=true)
     * @Groups({"wall"})
     */
    private $messages;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\People", inversedBy="wall", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"wall"})
     */
    private $people;

    public function __construct()
    {
        $this->messages = new ArrayCollection();
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setWall($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->contains($message)) {
            $this->messages->removeElement($message);
            // set the owning side to null (unless already changed)
            if ($message->getWall() === $this) {
                $message->setWall(null);
            }
        }

        return $this;
    }

    public function getPeople(): ?People
    {
        return $this->people;
    }

    public function setPeople(People $people): self
    {
        $this->people = $people;

        return $this;
    }
}
