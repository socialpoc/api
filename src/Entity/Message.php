<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(normalizationContext={"groups"={"all", "message"}})
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 */
class Message extends AbstractEntity
{
    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     * @Groups({"wall", "people"})
     */
    private $datetime;

    /**
     * @ORM\Column(type="string", length=4096, nullable=true)
     * @Groups({"wall", "people"})
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="People", inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"message", "wall"})
     */
    private $people;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Wall", inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $wall;

    /**
     * Message constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->datetime = new \DateTime();
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getPeople(): ?People
    {
        return $this->people;
    }

    public function setPeople(?People $people): self
    {
        $this->people = $people;

        return $this;
    }

    public function getWall(): ?Wall
    {
        return $this->wall;
    }

    public function setWall(?Wall $wall): self
    {
        $this->wall = $wall;

        return $this;
    }

}
