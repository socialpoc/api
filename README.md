# api

This project is based on symfony 4.3 and generated with [composer create-project symfony/website-sekeleton](https://github.com/symfony/website-skeleton)

You can find specs at https://gist.github.com/marmotz/a4b74bb372c4a18b629a022ec8fbb775


## Requirements

php 7.2 with sqlite support

## Build & development

Run `composer install` for building and `bin/console server:run` for preview.

note : launch in dev env.

## Get Started

### Database initialization

Run `bin/console doctrine:database:create` to generate sqlite database.

Run `bin/console doctrine:migrations:migrate` to generate schema and populate some data for tests.

### Test Api

a user in_memory called 'dude' with password 'white russian' can be used to generate a token (DO NOT USE this user to login from WEB UI)

use following command to generate a token :

`curl -X POST -H "Content-Type: application/json" http://localhost:8000/api/login_check -d '{"username":"dude","password":"white russian"}'`

The use this token as http header as described bellow (use [Modify HEaders for Google Chrome](https://chrome.google.com/webstore/detail/modify-headers-for-google/innpjfdalfhpcoinfnehdnbkglpmogdi) for example)

add following header
Authorization: 'Bearer YOUR_TOKEN'
Accept: 'application/json'

You will be allowed to GET some api URL like http://localhost:8000/api/peoples

WARNING : don't forget to disable headers when browsing API over WEB UI (else, your headers will override angularjs WEB headers).

## Testing

Running `bin/phpunit` will run the unit tests written in tests/ directory.
(first launch will install dependencies if necessary)

Note : There is no tests
