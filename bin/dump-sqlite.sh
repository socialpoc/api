#!/bin/bash

quit() {
    echo -e "ERROR : $1"
    exit 1
}
backup() {
    local file="$1"
    local date="`date -r $file "+%Y%m%d%H%M%S"`"
    mv -v $file $file-$date
}

out="dump.sql"
db_path="var/data.db"
sql_cmd="/usr/bin/sqlite3"

[ -x $sql_cmd ] || quit "sqlite cmd not found"
[ -e $out ] && backup $out
echo -e ".output $out\n.dump\n.exit\n" | $sql_cmd "$db_path"
