PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE migration_versions (version VARCHAR(255) NOT NULL, PRIMARY KEY(version));
INSERT INTO migration_versions VALUES('20181226141355');
CREATE TABLE wall (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, people_id INTEGER NOT NULL, CONSTRAINT FK_13F5EFF63147C936 FOREIGN KEY (people_id) REFERENCES people (id) NOT DEFERRABLE INITIALLY IMMEDIATE);
INSERT INTO wall VALUES(1,1);
INSERT INTO wall VALUES(2,2);
INSERT INTO wall VALUES(3,3);
INSERT INTO wall VALUES(4,4);
INSERT INTO wall VALUES(5,5);
INSERT INTO wall VALUES(6,6);
CREATE TABLE message (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, people_id INTEGER NOT NULL, wall_id INTEGER NOT NULL, datetime DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, content CLOB DEFAULT NULL, CONSTRAINT FK_B6BD307F3147C936 FOREIGN KEY (people_id) REFERENCES people (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_B6BD307FC33923F1 FOREIGN KEY (wall_id) REFERENCES wall (id) NOT DEFERRABLE INITIALLY IMMEDIATE);
INSERT INTO message VALUES(1,1,1,'2018-12-21 17:02:54','Si ca s trouve ? Alors pour nous sortir de la il va nous falloir un peu plus solide que du si ca s trouve');
CREATE TABLE people (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username VARCHAR(255) NOT NULL COLLATE BINARY, password CLOB NOT NULL);
INSERT INTO people VALUES(1,'sylvain','$2y$13$7mHpG5NReVkcyDjXAQKeted0hSKoqa5LD/q.Ces.S59YncvvKC3vi');
INSERT INTO people VALUES(2,'renaud','$2y$13$7mHpG5NReVkcyDjXAQKeted0hSKoqa5LD/q.Ces.S59YncvvKC3vi');
INSERT INTO people VALUES(3,'cedric','$2y$13$7mHpG5NReVkcyDjXAQKeted0hSKoqa5LD/q.Ces.S59YncvvKC3vi');
INSERT INTO people VALUES(4,'foobar','$2y$13$7mHpG5NReVkcyDjXAQKeted0hSKoqa5LD/q.Ces.S59YncvvKC3vi');
INSERT INTO people VALUES(5,'arthur','$2y$13$7mHpG5NReVkcyDjXAQKeted0hSKoqa5LD/q.Ces.S59YncvvKC3vi');
INSERT INTO people VALUES(6,'sandrine','$2y$13$7mHpG5NReVkcyDjXAQKeted0hSKoqa5LD/q.Ces.S59YncvvKC3vi');
CREATE TABLE people_people (people_source INTEGER NOT NULL, people_target INTEGER NOT NULL, PRIMARY KEY(people_source, people_target), CONSTRAINT FK_5E0148CC299D5D99 FOREIGN KEY (people_source) REFERENCES people (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_5E0148CC30780D16 FOREIGN KEY (people_target) REFERENCES people (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE);
INSERT INTO people_people VALUES(1,2);
INSERT INTO people_people VALUES(2,1);
DELETE FROM sqlite_sequence;
INSERT INTO sqlite_sequence VALUES('wall',6);
INSERT INTO sqlite_sequence VALUES('message',1);
INSERT INTO sqlite_sequence VALUES('people',6);
CREATE UNIQUE INDEX UNIQ_28166A26F85E0677 ON people (username);
CREATE UNIQUE INDEX UNIQ_13F5EFF63147C936 ON wall (people_id);
CREATE INDEX IDX_B6BD307FC33923F1 ON message (wall_id);
CREATE INDEX IDX_B6BD307F3147C936 ON message (people_id);
CREATE INDEX IDX_5E0148CC30780D16 ON people_people (people_target);
CREATE INDEX IDX_5E0148CC299D5D99 ON people_people (people_source);
COMMIT;
